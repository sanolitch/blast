export class AnimatorBuilder {

  constructor() {
    this.isLoop = false;
    this.totalFrames = 0;
    this.isPlaying = false;
    this.animationSpeed = 0;
  }

  set IsLoop(value) {
    this.isLoop = value;
  }

  set TotalFrames(value) {
    this.totalFrames = value - 1;
  }

  set IsPlaying(value) {
    this.isPlaying = value;
  }

  set AnimationSpeed(value) {
    this.animationSpeed = value;
  }

}
