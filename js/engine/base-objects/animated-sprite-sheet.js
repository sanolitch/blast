import { SpriteSheet } from "./sprite-sheet";
import { Animator } from "./animator";

export class AnimatedSpriteSheet extends SpriteSheet {

  constructor(transform, animatorBuilder) {
    super(transform);

    this.animator = new Animator(animatorBuilder);
  }

  AnimationWork(deltaTime) {
    if (!this.animator.isPlaying) {
      return;
    }
    this.animator.frame += this.animator.animationSpeed * deltaTime;
    super.activeSprite = Math.round(this.animator.frame - 0.5);

    if (this.animator.Finished && this.animator.isLoop) {
      this.activeSprite = 0;
      this.animator.frame = 0;
    }
    if (this.animator.Finished && !this.animator.isLoop) {
      super.activeSprite = 0;

      this.animator.Stop();
    }
  }

  Render(deltaTime) {
    this.AnimationWork(deltaTime);

    super.Render(deltaTime);
  }

}
