import { Position } from "../structures/position";
import { Velocity } from "../structures/velocity";

export class Transform {

  constructor() {
    this.position = new Position(0, 0, 0);
    this.newPosition = new Position(0, 0, 0);
    this.width = 0;
    this.height = 0;
    this.velocity = new Velocity(0, 0, 0);
  }

  get X() {
    return this.position.x;
  }

  get Y() {
    return this.position.y;
  }

  get Z() {
    return this.position.z;
  }

  get Width() {
    return this.width;
  }

  get Height() {
    return this.height;
  }

  SetPosition(position) {
    this.position = position;
    this.newPosition = position;
  }

  SetSize(width, height) {
    this.width = width;
    this.height = height;
  }

  Move(position, velocityValue) {
    this.newPosition = position;

    const directionX = position.x - this.position.x > 0 ? 1 : -1;
    const directionY = position.y - this.position.y > 0 ? 1 : -1;

    this.velocity = new Velocity(velocityValue, directionX, directionY);
  }

  SetPositionForFrame(axis, deltaTime) {
    const direction =  this.velocity['direction' + axis.toUpperCase()];
    const diff = (this.newPosition[axis] - this.position[axis]) * direction > 0

    if (diff) {
      this.position[axis] += this.velocity.value * deltaTime * direction;
    } else {
      this.position[axis] = this.newPosition[axis];
    }
  }

  Moving(deltaTime) {
    const isShouldBeMoved = this.position.x !== this.newPosition.x ||
                            this.position.y !== this.newPosition.y;

    if (!isShouldBeMoved) {
      return;
    }
    this.SetPositionForFrame('x', deltaTime);
    this.SetPositionForFrame('y', deltaTime);
  }

}
