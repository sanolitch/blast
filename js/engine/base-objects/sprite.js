import { Engine } from "../engine";

export class Sprite {

  constructor(transform) {
    this.canvas = Engine.canvas;
    this.ctx = Engine.ctx;
    this.image = new Image();
    this.transform = transform;
    this.isRender = true;
  }

  SetImagePath(path) {
    this.image.src = path;
  }

  Render(deltaTime) {
    const isRenderAvailable = this.image && this.isRender;

    if (!isRenderAvailable) {
      return;
    }
    this.ctx.drawImage(
      this.image,
      this.transform.position.x,
      this.transform.position.y,
      this.transform.width,
      this.transform.height
    );
  }

  StopRender() {
    this.isRender = false;
  }

  StartRender() {
    this.isRender = true;
  }
}
