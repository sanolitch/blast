import { GenerateUUID } from "../utils/common-utils";

export class Scene {

  constructor() {
    this.sceneObjects = new Map();
  }

  AddObjToScene(obj, name = null) {
    if (!name) {
      name = GenerateUUID();
    }
    this.sceneObjects.set(name, obj);
  }

}


