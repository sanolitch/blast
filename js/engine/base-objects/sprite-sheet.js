import { Sprite } from "./sprite";

export class SpriteSheet extends Sprite {

  constructor(transform) {
    super(transform);

    this.frameWidth = 0;
    this.frameHeight = 0;
    this.activeSprite = 0;
  }

  Render(deltaTime) {
    const isRenderAvailable = this.image && this.isRender;

    if (!isRenderAvailable) {
      return;
    }
    this.ctx.drawImage(
      this.image,
      this.frameWidth * this.activeSprite,
      0,
      this.frameWidth,
      this.frameHeight,
      this.transform.position.x,
      this.transform.position.y,
      this.transform.width,
      this.transform.height
    )
  }

}
