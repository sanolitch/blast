import { Transform } from "./transform";
import { Sprite } from "./sprite";

export class BaseSceneObject {

  constructor() {
    this.Initialize();
  }

  get Transform() {
    return this.transform;
  }

  Initialize() {
    this.transform = new Transform();
    this.sprite = new Sprite(this.transform);
  }

  SetPosition(position) {
    this.transform.SetPosition(position);
  }

  SetSize(width, height) {
    this.transform.SetSize(width, height);
  }

  SetImagePath(path) {
    this.sprite.SetImagePath(path);
  }

  Update(deltaTime) {
    this.transform.Moving(deltaTime);
    this.sprite.Render(deltaTime);
  }

  StartRender() {
    this.sprite.StartRender();
  }

  Destroy() {
    this.sprite.StopRender();
  }

}
