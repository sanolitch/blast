export class Animator {

  constructor(animatorBuilder) {
    this.isLoop = animatorBuilder.isLoop;
    this.totalFrames = animatorBuilder.totalFrames;
    this.isPlaying = animatorBuilder.isPlaying;
    this.animationSpeed = animatorBuilder.animationSpeed;
    this.frame = 0;
    this.callback = null;
  }

  get Finished() {
    return this.frame >= this.totalFrames;
  }

  Play(callback = null) {
    this.isPlaying = true;
    this.callback = callback;
  }

  PlayOnce(callback = null) {
    this.frame = 0;
    this.isPlaying = true;
    this.isLoop = false;
    this.callback = callback;
  }

  Pause() {
    this.isPlaying = false;
  }

  Stop() {
    this.frame = 0;
    this.isLoop = false;

    if (this.callback) {
      this.callback();

      this.callback = null;
    }
    this.Pause();
  }

}
