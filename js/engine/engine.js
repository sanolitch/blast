import { EngineSettings } from "./engine-settings";
import { InputManager } from "./managers/input-manager";
import { RendererBase } from "./renderer";
import { UiManager } from "./managers/ui-manager";

export let Engine;
export let Input;
export let Renderer;
export let Ui;

let Settings;

class EngineBase {

  constructor() {
    this.CreateCanvas();
    requestAnimationFrame(this.Update.bind(this));
  }

  CreateCanvas() {
    this.canvas = document.createElement('canvas');
    this.canvas.id = 'canvas';
    this.canvas.width = Settings.totalWidth;
    this.canvas.height = Settings.totalHeight;
    this.ctx = this.canvas.getContext('2d');
    this.backgroundColor = Settings.canvasBackground;

    const container = document.getElementById('game-container');

    container.appendChild(this.canvas);
    window.requestAnimationFrame(this.Update.bind(this));
  }

  ClearCanvas() {
    this.ctx.fillStyle = this.backgroundColor;

    this.ctx.fillRect(0, 0, Settings.totalWidth, Settings.totalHeight);
  }

  Update() {
    this.ClearCanvas();

    requestAnimationFrame(this.Update.bind(this));
  }

}

export function InitEngine() {
  Settings = new EngineSettings();
  Engine = new EngineBase();
  Input = new InputManager();
  Renderer = new RendererBase();
  Ui = new UiManager();
}




