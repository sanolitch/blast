export class RendererBase {

  constructor() {
    this.scenes = new Map();
    this.activeScenes = new Set();

    requestAnimationFrame(this.Update.bind(this, new Date().getTime()));
  }

  AddScene(name, sceneObj) {
    this.scenes.set(name, sceneObj);
  }

  SortSceneObjects(sceneObjects) {
    const result = [];

    for (const obj of sceneObjects) {
      result.push(obj);
    }
    return result
      .sort((a, b) => b.Transform.position.z > a.Transform.position.z ? -1 : 1);
  }

  RenderScene(sceneName, deltaTime) {
    if (!this.scenes.has(sceneName)) {
      return;
    }
    const activeScene = this.scenes.get(sceneName);
    const sceneObjects = activeScene.sceneObjects.values();
    const sortedSceneObjects = this.SortSceneObjects(sceneObjects);

    sortedSceneObjects
      .map(sceneObject => sceneObject.Update(deltaTime));
  }

  Update(timestamp) {
    const newTimestamp = new Date().getTime();
    const deltaTime = (newTimestamp - timestamp) / 8;

    for (const sceneName of this.activeScenes) {
      if (sceneName === 'onTop') {
        continue;
      }
      this.RenderScene(sceneName, deltaTime);
    }
    this.RenderScene('onTop', deltaTime);

    requestAnimationFrame(this.Update.bind(this, newTimestamp));
  }

  SetActiveScene(sceneName) {
    this.activeScenes.add(sceneName);
  }

  ClearActiveScenes() {
    this.activeScenes.clear();
  }

}
