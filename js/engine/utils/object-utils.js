export function IsNull(obj) {
  return obj === null || obj === undefined;
}
export function ReplaceNull(obj, replaceValue = null) {
  return IsNull(obj) ? replaceValue : obj;
}

export function TrySetValue(obj, property, value) {
  if (!IsNull(obj) && obj.hasOwnProperty(property)) {
    obj[property] = value;
  }
}
export function TryGetValue(obj, property) {
  if (!IsNull(obj) && obj.hasOwnProperty(property)) {
    return obj[property];
  }
  return null;
}

