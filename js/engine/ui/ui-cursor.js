import { BaseSceneObject } from "../base-objects/base-scene-object";
import { Input } from "../engine";

export class UiCursor extends BaseSceneObject {

  constructor() {
    super();

    this.sprite.StopRender();
  }

  Update(deltaTime) {
    super.Update(deltaTime);

    this.SetPosition({ x: Input.mousePosX, y: Input.mousePosY });
  }

  Switch(imgPath = null) {
    this.sprite.isRender = !this.sprite.isRender && imgPath;

    if (imgPath) {
      this.SetImagePath(imgPath);
    }
  }

}
