import { Engine } from "../../engine";

export class UiText {

  constructor(text, textBuilder) {
    this.ctx = Engine.ctx;

    this.font = textBuilder.font;
    this.fontSize = textBuilder.fontSize;
    this.fontColor = textBuilder.fontColor;
    this.textAlign = textBuilder.textAlign;
    this.textPosition = textBuilder.textPosition;
    this.text = text;
    this.isRender = true;
  }

  set Text(textValue) {
    this.text = textValue;
  }

  Render() {
    const isRenderAvailable = this.text && this.isRender;

    if (!isRenderAvailable) {
      return;
    }
    this.ctx.font = `${ this.fontSize } ${ this.font }`;
    this.ctx.fillStyle = this.fontColor;
    this.ctx.textAlign = this.textAlign;
    this.ctx.fillText(this.text, this.textPosition.x, this.textPosition.y);
  }

  Update(timestamp) {
    this.Render();
  }

  SetPosition(position) {
    this.textPosition = position;
  }

  StopRender() {
    this.isRender = false;
  }

  StartRender() {
    this.isRender = true;
  }

  Destroy() {
    this.StopRender();
  }

}
