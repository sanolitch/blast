import { Position } from "../../structures/position";

export class TextBuilder {

  constructor() {
    this.font = 'Marvin';
    this.fontSize = '30px';
    this.fontColor = 'white';
    this.textAlign = 'left';
    this.textPosition = new Position(0, 25, 0);
  }

  set Font(font) {
    this.font = font;
  }

  set FontSize(fontSize) {
    this.fontSize = fontSize;
  }

  set FontColor(fontColor) {
    this.fontColor = fontColor;
  }

  set TextAlign(textAlign) {
    this.textAlign = textAlign;
  }

  set TextPosition(textPosition) {
    this.textPosition = textPosition;
  }

}
