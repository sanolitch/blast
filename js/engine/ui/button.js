import { UiCompositeObject } from "./ui-composite-object";
import { UiText } from "./ui-text/ui-text";
import { TextBuilder } from "./ui-text/text-builder";
import { Input, Ui } from "../engine";

export class UiButton extends UiCompositeObject {

  constructor(isModalBehaviour = false, width = 0, height = 0, position = null) {
    super();

    this.text = new UiText('', new TextBuilder());
    this.isModalBehaviour = isModalBehaviour;

    this.SubscribeCanvasClick();
  }

  OnButtonClick() {
    if (Ui.isModalOpened && !this.isModalBehaviour) {
      return false;
    }
    console.info('button clicked!');

    return true;
  }

  CheckClick() {
    const mouseX = Input.mousePosX;
    const mouseY = Input.mousePosY;
    const buttonX = this.container.transform.X;
    const buttonWidth = this.container.transform.Width;
    const buttonY = this.container.transform.Y;
    const buttonHeight = this.container.transform.Height;
    const isMouseUnderButton =
      mouseX >= buttonX &&
      mouseX <= buttonX + buttonWidth &&
      mouseY >= buttonY &&
      mouseY <= buttonY + buttonHeight

    if (isMouseUnderButton) {
      this.OnButtonClick();
    }
  }

  CreateTextObj(text, position = null, fontSize = '30px') {
    this.text = super.CreateTextObj(text, position, fontSize);

    return this.text;
  }

  Destroy() {
    this.text.Destroy();
    this.container.Destroy();
  }

  SubscribeCanvasClick() {
    addEventListener('canvasClick', this.CheckClick.bind(this))
  }

}
