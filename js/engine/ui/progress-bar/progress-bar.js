import { BaseSceneObject } from "../../base-objects/base-scene-object";

export class ProgressBar {

  constructor(width, height, position = null) {
    this.value = 0;

    this.capacity = new BaseSceneObject();
    this.filling = new BaseSceneObject();

    this.capacity.SetSize(width, height);
    this.filling.SetSize(this.value, height);
  }

  SetImagePaths(capacityPath, fillingPath) {
    this.capacity.SetImagePath(capacityPath);
    this.filling.SetImagePath(fillingPath);
  }

  set Value(value) {
    this.value = value;
    this.filling.SetSize(this.CalcFillingWidth(), this.filling.transform.height);
  }

  CalcFillingWidth() {
    return this.capacity.transform.width / 100 * this.value;
  }

  SetPosition(position) {
    this.capacity.SetPosition(position);
    this.filling.SetPosition(position);
  }

  SetSize(width, height) {
    this.capacity.SetSize(width, height);
    this.filling.SetSize(width, height);

    this.Value = 0;
  }

  Update(deltaTime) {
    this.capacity.Update(deltaTime);
    this.filling.Update(deltaTime);
  }

}
