import { BaseSceneObject } from "../base-objects/base-scene-object";
import { Position } from "../structures/position";

export class UiContainer extends BaseSceneObject {

  constructor(width, height, position) {
    super();

    this.children = [];

    this.SetSize(width, height);

    if (position) {
      this.SetPosition(position);
    }
  }

  AddObject(object, position, widthPercent, heightPercent) {
    const objX = this.transform.X + position.x;
    const objY = this.transform.Y + position.y;
    const objZ = this.transform.Z + 1;

    const objPosition = new Position(objX, objY, objZ);

    object.SetPosition(objPosition);

    if (widthPercent && heightPercent) {
      object.SetSize(widthPercent / 100 * this.transform.Width, heightPercent / 100 * this.transform.Height);
    }
    this.children.push(object);
  }

  Update(deltaTime) {
    super.Update(deltaTime);

    this.children.map(child => child.Update(deltaTime));
  }

}
