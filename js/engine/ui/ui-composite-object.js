import { UiContainer } from "./ui-container";
import { TextBuilder } from "./ui-text/text-builder";
import { UiText } from "./ui-text/ui-text";

export class UiCompositeObject {

  constructor(width = 0, height = 0, position = null) {
    this.container = new UiContainer(width, height, position);
  }

  get Transform() {
    return this.container.transform;
  }

  CreateTextObj(text, position = null, fontSize = '30px') {
    const textBuilder = new TextBuilder();

    if (position) {
      position.x += this.container.transform.X;
      position.y += this.container.transform.Y;
    }
    textBuilder.FontColor = 'white';
    textBuilder.Font = 'Marvin';
    textBuilder.FontSize = fontSize;
    textBuilder.TextPosition = position;

    const textObj = new UiText(text, textBuilder);

    this.container.children.push(textObj)

    return textObj;
  }

  SetPosition(position) {
    this.container.SetPosition(position);
  }

  SetSize(width, height) {
    this.container.SetSize(width, height);
  }

  Update(deltaTime) {
    this.container.Update(deltaTime);
  }

}
