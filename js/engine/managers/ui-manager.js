import { Scene } from "../base-objects/scene";
import { Renderer } from "../engine";
import { ObjectManagerInstance } from "../../main";

export class UiManager {

  constructor() {
    const topLayerScene = new Scene();

    Renderer.AddScene('onTop', topLayerScene);
    Renderer.SetActiveScene('onTop');

    this.isModalOpened = false;
  }

  ShowPopup(message, popupBuilder, isModal = false) {
    const popup = ObjectManagerInstance.GetObject('onTop', 'popup');

    popup.SetSetting(popupBuilder);
    popup.ShowModal(message, this.ClosePopup.bind(this));

    this.isModalOpened = isModal;
  }

  ClosePopup() {
    this.isModalOpened = false;
  }

  SwitchCursor(imgPath = null) {
    const cursor = ObjectManagerInstance.GetObject('onTop', 'bombCursor');

    cursor.Switch(imgPath);
  }

}
