import { Scene } from "../base-objects/scene";
import { Renderer } from "../engine";

export class ObjectManager {

  constructor() {
  }

  CreateScene(name, isActive = false) {
    try {
      if (name === 'onTop') {
        throw '"onTop" reserved as scene name. Please choose another name for your scene.';
      }
      const newScene = new Scene();

      Renderer.AddScene(name, newScene);

      if (isActive) {
        Renderer.SetActiveScene(name);
      }
      return newScene;
    } catch (e) {
      console.error(e);
    }
  }

  GetScene(sceneName) {
    if (!Renderer.scenes.has(sceneName)) {
      return null;
    }
    return Renderer.scenes.get(sceneName);
  }

  GetObject(sceneName, objectName) {
    const scene = this.GetScene(sceneName);

    if (!scene) {
      return null;
    }
    return scene.sceneObjects.get(objectName);
  }

}
