import { Engine } from "../engine";

export class InputManager {

  constructor() {
    this.canvas = Engine.canvas;
    this.mousePosX = 0;
    this.mousePosY = 0;
    this.clickEvent = new CustomEvent('canvasClick');

    this.SubscribeToInputEvents();
  }

  SubscribeToInputEvents() {
    const leftOffset = this.canvas.offsetLeft + this.canvas.clientLeft;
    const topOffset = this.canvas.offsetTop + this.canvas.clientTop;

    this.canvas.addEventListener('click', () => {
      dispatchEvent(this.clickEvent);
    });
    this.canvas.addEventListener('mousemove', (event) => {
      this.mousePosX = event.pageX - leftOffset;
      this.mousePosY = event.pageY - topOffset;
    })
  }

}


