export class EngineSettings {

  constructor() {
    const windowSize = this.GetWindowSize();

    this.totalWidth = windowSize.width;
    this.totalHeight = windowSize.height;
    this.canvasBackground = '#8ec7e9';
  }

  GetWindowSize() {
    let win = window;
    let doc = document;
    let docElem = doc.documentElement;
    let body = doc.body;
    let width = win.innerWidth || docElem.clientWidth || body.clientWidth;
    let height = win.innerHeight|| docElem.clientHeight|| body.clientHeight;

    return { width, height };
  }

}
