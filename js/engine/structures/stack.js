export class Stack {

  constructor() {
    this.storage = [];
  }

  Size() {
    return this.storage.length;
  }

  Push(element) {
    this.storage.push(element);
  }

  Pop() {
    if (this.Size() === 0) {
      return null;
    }
    const result = this.storage[0];

    this.storage.splice(0, 1);

    return result;
  }

  Peek() {
    if (this.Size() === 0) {
      return null;
    }
    return this.storage[0];
  }

  Concat(other) {
    this.storage = this.storage.concat(other.storage);

    return this;
  }

}
