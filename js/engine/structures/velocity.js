export class Velocity {

  constructor(value, directionX, directionY) {
    this.value = value;
    this.directionX = directionX;
    this.directionY = directionY;
  }

}
