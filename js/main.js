import { GameManager } from "./game/game-manager";
import { BoosterManager } from "./game/booster-manager";
import { InitEngine } from "./engine/engine";
import { PopupManager } from "./game/popup-manager";
import { Field } from "./game/game-objects/field";
import { ProgressPanel } from "./game/ui/progress-panel";
import { ScorePanel } from "./game/ui/score-panel";
import { BombButton } from "./game/ui/bomb-button";
import { ShuffleButton } from "./game/ui/shuffle-button";
import { ObjectManager } from "./engine/managers/object-manager";
import { GridManager } from "./game/grid/grid-manager";
import { UiCursor } from "./engine/ui/ui-cursor";
import { PopupWindow, PopupWindowBuilder } from "./game/ui/popup-window";
import { JesusBombButton } from "./game/ui/jesus-bomb-button";

export let GameManagerInstance;
export let ObjectManagerInstance;
export let GridManagerInstance;
export let BoosterManagerInstance;

function CreateMainScene() {
  const mainScene = ObjectManagerInstance.CreateScene('main', true);
  const field = new Field();

  GridManagerInstance.field = field;
  GridManagerInstance.grid = field.cubesGrid;

  field.cubesGrid.InitializeGrid();

  mainScene.AddObjToScene(field, 'field');
}

function CreateUiScene() {
  const uiScene = ObjectManagerInstance.CreateScene('ui', true);

  uiScene.AddObjToScene(new ProgressPanel());
  uiScene.AddObjToScene(new ScorePanel());
  uiScene.AddObjToScene(new BombButton());
  uiScene.AddObjToScene(new ShuffleButton());
  uiScene.AddObjToScene(new JesusBombButton());
}

function CreateSceneForAlwaysOnTopObjects() {
  const onTopScene = ObjectManagerInstance.GetScene('onTop');
  const bombCursor = new UiCursor();

  bombCursor.SetImagePath('img/ui/bomb.png');
  bombCursor.SetSize(80, 80);

  const popupBuilder = new PopupWindowBuilder();
  const popupWindow = new PopupWindow(popupBuilder);

  onTopScene.AddObjToScene(bombCursor, 'bombCursor');
  onTopScene.AddObjToScene(popupWindow, 'popup');
}

document.body.onload = () => {
  InitEngine();
  GameManagerInstance = new GameManager();

  const popupManager = new PopupManager();

  GridManagerInstance = new GridManager();
  ObjectManagerInstance = new ObjectManager();
  BoosterManagerInstance = new BoosterManager();

  CreateMainScene();
  CreateUiScene();
  CreateSceneForAlwaysOnTopObjects();
};



