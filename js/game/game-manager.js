import { BoosterManagerInstance, GridManagerInstance } from "../main";

export class GameManager {

  constructor() {
    this.score = 0;
    this.turnsLeft = 20;
    this.goal = 20;
    this.level = 1;

    GameManager.Subscriptions = {};

    GameManager.Subscribe('score', this.CheckConditions.bind(this));
  }

  static Subscribe(channel, callback) {
    if (!this.Subscriptions[channel]) {
      this.Subscriptions[channel] = [];
    }
    this.Subscriptions[channel].push(callback);
  }

  static Publish(channel, data) {
    const subscriptions = this.Subscriptions[channel];

    if (!subscriptions || !subscriptions.length) {
      return;
    }
    subscriptions.forEach(callback => callback(data))
  }

  Turn(score) {
    this.score += score;
    this.turnsLeft -= 1;

    GameManager.Publish('score', score);
  }

  Win() {
    this.score = 0;
    this.turnsLeft = 20;
    this.level += 1;
    this.goal += 5;
    BoosterManagerInstance.Bombs += this.level % 3 ? 0 : 1;
    BoosterManagerInstance.Shuffles += this.level % 5 ? 0 : 1;
    BoosterManagerInstance.JesusBombs += this.level % 6 ? 0 : 1;

    GridManagerInstance.Shuffle();
    GameManager.Publish('win', null);
  }

  Loose() {
    this.score = 0;
    this.level = 1;
    this.turnsLeft = 20;
    this.goal = 20;
    BoosterManagerInstance.Bombs = 1;
    BoosterManagerInstance.Shuffles = 1;
    BoosterManagerInstance.JesusBombs = 1;

    GridManagerInstance.Shuffle();
    GameManager.Publish('loose', null);
  }

  CheckConditions() {
    const winCondition = this.turnsLeft >= 0 && this.score >= this.goal;

    if (winCondition) {
      this.Win();
    }
    const looseCondition = this.turnsLeft <= 0 && this.score < this.goal;

    if (looseCondition) {
      this.Loose();
    }
  }

}
