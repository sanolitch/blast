import { PopupWindowBuilder } from "./ui/popup-window";
import { GameManager } from "./game-manager";
import { Ui } from "../engine/engine";
import { GameManagerInstance } from "../main";

export class PopupManager {

  constructor() {
    GameManager.Subscribe('win', () => {
      const popupBuilder = new PopupWindowBuilder();

      popupBuilder.width += 20;

      Ui.ShowPopup(`Поздравляем! Ты достиг ${ GameManagerInstance.level } уровня!`, popupBuilder, true);
    });
    GameManager.Subscribe('loose', () => {
      const popupBuilder = new PopupWindowBuilder();

      Ui.ShowPopup('Ты проиграл T_T Попробуй еще раз!', popupBuilder, true);
    });
  }

}
