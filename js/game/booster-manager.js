import { GridManagerInstance } from "../main";
import { Ui } from "../engine/engine";
import { GameManager } from "./game-manager";

export const BoosterType = Object.freeze(
  {
    Bomb: 1,
    JesusBomb: 2,
    Shuffle: 3
  }
)

export class BoosterManager {

  constructor() {
    this.boosters = new Map();

    this.boosters.set(BoosterType.Bomb, 1);
    this.boosters.set(BoosterType.JesusBomb, 1);
    this.boosters.set(BoosterType.Shuffle, 1);

    this.currentBooster = null;
  }

  get Bombs() {
    return this.boosters.get(BoosterType.Bomb);
  }

  get Shuffles() {
    return this.boosters.get(BoosterType.Shuffle);
  }

  get JesusBombs() {
    return this.boosters.get(BoosterType.JesusBomb);
  }

  get CurrentBooster() {
    return this.currentBooster;
  }

  set Bombs(value) {
    this.boosters.set(BoosterType.Bomb, value);
  }

  set Shuffles(value) {
    this.boosters.set(BoosterType.Shuffle, value);
  }

  set JesusBombs(value) {
    this.boosters.set(BoosterType.JesusBomb, value);
  }

  ChargeBomb() {
    if (this.Bombs <= 0) {
      return;
    }
    this.currentBooster = BoosterType.Bomb;

    Ui.SwitchCursor('img/ui/cursors/bomb.png');
  }

  ChargeJesusBomb() {
    if (this.JesusBombs <= 0) {
      return;
    }
    this.currentBooster = BoosterType.JesusBomb;

    Ui.SwitchCursor('img/ui/cursors/jesus_bomb.png');
  }

  ActivateBooster() {
    const newValue = this.boosters.get(this.currentBooster);

    this.boosters.set(this.currentBooster, newValue - 1);

    this.currentBooster = null;

    GameManager.Publish('booster');

    Ui.SwitchCursor();
  }

  DefuseBomb() {
    this.currentBooster = null;

    Ui.SwitchCursor();
  }

  Shuffle() {
    if (this.Shuffles <= 0) {
      return;
    }
    this.Shuffles--;
    this.ActivateBooster();

    GridManagerInstance.Shuffle();
  }

}
