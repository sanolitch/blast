import { CellPosition } from "./cell-position";
import { Input, Ui } from "../../engine/engine";
import { BaseSceneObject } from "../../engine/base-objects/base-scene-object";
import { GridManagerInstance } from "../../main";
import { Column } from "./column";

export class CubesGrid extends BaseSceneObject {

  constructor(field) {
    super();

    this.field = field;
    this.cells = [];

    this.SubscribeCanvasClick();
  }

  InitializeGrid() {
      for (let columnIndex = 0; columnIndex < this.field.columns; columnIndex++) {
        const column = new Column(columnIndex, this.field.rows);

        this.cells.push(column);
      }
    console.info('cells', this.cells);
  }

  IsInGridRange(columnIndex, rowIndex) {
    return columnIndex < this.cells.length && rowIndex < this.cells[columnIndex].Count;
  }

  GetCellAt(mouseX, mouseY) {
    const columnIndex = Math.round(((mouseX - this.field.LeftBorderX) / this.field.cubeWidth) - 0.5);
    const rowIndex = Math.round(((mouseY - this.field.LeftBorderY) / this.field.cubeHeight) - 0.5);
    const isInGridRange = this.IsInGridRange(columnIndex, rowIndex);

    if (!isInGridRange) {
      return null;
    }
    const cellPosition = new CellPosition(columnIndex, rowIndex);

    return this.GetCell(cellPosition);
  }

  GetCell(position) {
    return this.cells[position.ColumnIndex].GetItem(position.RowIndex);
  }

  OnCellClick(mouseX, mouseY) {
    const cell = this.GetCellAt(mouseX, mouseY);

    if (!cell || !cell.Cube) {
      return;
    }
    GridManagerInstance.BurnCubes(cell);
  }

  SubscribeCanvasClick() {
    const instance = this;

    addEventListener('canvasClick', () => {
      if (Ui.isModalOpened) {
        return;
      }
      const mousePosX = Input.mousePosX;
      const mousePosY = Input.mousePosY;
      const isFieldClickedCheckX = mousePosX >= instance.field.fieldPositionX &&
        mousePosX <= instance.field.fieldPositionX + instance.field.Width
      const isFieldClickedCheckY = mousePosY >= instance.field.fieldPositionY &&
        mousePosY <= instance.field.fieldPositionY + instance.field.Height

      if (isFieldClickedCheckX && isFieldClickedCheckY) {
        console.info('Field clicked!', { mousePosX, mousePosY });

        this.OnCellClick(mousePosX, mousePosY);
      }
    });
  }

  Update(deltaTime) {
    for (const column of this.cells) {
      for (const cell of column.Items) {
        if (!cell.cube) {
          continue;
        }
        cell.cube.Update(deltaTime);
      }
    }
  }

}
