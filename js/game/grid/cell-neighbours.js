import { CellPosition } from "./cell-position";
import { GridManagerInstance } from "../../main";

export class CellNeighbours {

  constructor(currentCellColumnIndex, currentCellRowIndex) {
    this.leftCell = null;
    this.topCell = null;
    this.rightCell = null;
    this.bottomCell = null;

    this.SetNeighbours(currentCellColumnIndex, currentCellRowIndex);
  }

  SetNeighbours(currentCellColumnIndex, currentCellRowIndex) {
    const field = GridManagerInstance.field;

    if (currentCellColumnIndex > 0) {
      this.leftCell = new CellPosition(currentCellColumnIndex - 1, currentCellRowIndex);
    }
    if (currentCellRowIndex > 0) {
      this.topCell = new CellPosition(currentCellColumnIndex, currentCellRowIndex - 1);
    }
    if (currentCellColumnIndex < field.columns - 1) {
      this.rightCell = new CellPosition(currentCellColumnIndex + 1, currentCellRowIndex);
    }
    if (currentCellRowIndex < field.rows - 1) {
      this.bottomCell = new CellPosition(currentCellColumnIndex, currentCellRowIndex + 1);
    }
  }

}
