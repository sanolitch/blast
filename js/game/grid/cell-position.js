export class CellPosition {

  constructor(columnIndex, rowIndex) {
    this.columnIndex = columnIndex;
    this.rowIndex = rowIndex;
  }

  get ColumnIndex() {
    return this.columnIndex;
  }

  get RowIndex() {
    return this.rowIndex;
  }

}
