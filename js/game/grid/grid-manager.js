import { CellPosition } from "./cell-position";
import { IsNull } from "../../engine/utils/object-utils";
import { MathUtils } from "../../engine/utils/math-utils";
import { BoosterType } from "../booster-manager";
import { Stack } from "../../engine/structures/stack";
import { BoosterManagerInstance, GameManagerInstance } from "../../main";
import { SuperTile } from "../game-objects/super-tile";

export class GridManager {

  constructor() {
    this.grid = null;
    this.field = null;
  }

  Shuffle() {
    for (let row = 0; row < this.field.rows; row++) {
      for (let column = 0; column < this.field.columns; column++) {
        const cellPosition = new CellPosition(column, row);
        const cell = this.grid.GetCell(cellPosition);
        const cellCube = cell.Cube;
        const newRow = MathUtils.Random(0, this.field.rows);
        const newColumn = MathUtils.Random(0, this.field.columns);
        const cellPosition2 = new CellPosition(newColumn, newRow);
        const cell2 = this.grid.GetCell(cellPosition2);
        const cell2Cube = cell2.Cube;

        cell.cube = cell2Cube;
        cell2.cube = cellCube;
        cell.cube.cell = cell;
        cell2.cube.cell = cell2;

        cell.cube.UpdatePosition();
        cell2.cube.UpdatePosition();
      }
    }
  }

  MoveCubes(columnsWithGaps) {
    for (const columnIndex of columnsWithGaps) {
      this.grid.cells[columnIndex].PushItems();
    }
    console.info('Grid after moving', this.grid.cells);
  }

  // TODO: remove indexOf
  GetNeighboursWithTheSameColor(originCell) {
    const cellsStack = new Stack();
    const resultStack = new Stack();

    cellsStack.Push(originCell);
    resultStack.Push(originCell);

    while (cellsStack.Size() > 0) {
      const currentCell = cellsStack.Pop();
      const neighbours = currentCell.Neighbours;

      for (const neighbourPropName in neighbours) {
        const neighbourCellPosition = neighbours[neighbourPropName];

        if (IsNull(neighbourCellPosition)) {
          continue;
        }
        const neighbourCell = this.grid.GetCell(neighbourCellPosition);
        const neighbourCube = neighbourCell.Cube;
        const neighbourIsSuitable =
          neighbourCube &&
          currentCell.Cube.color === neighbourCube.color &&
          resultStack.storage.indexOf(neighbourCell) === -1;

        if (neighbourIsSuitable) {
          cellsStack.Push(neighbourCell);
          resultStack.Push(neighbourCell);
        }
      }
    }
    return resultStack;
  }

  DestroyCubes(cells) {

    const destroyCubesPromises = [];
    const columnsWithGaps = new Set();
    const cellsCount = cells.Size()

    while (cells.Size() > 0) {
      const cell = cells.Pop();

      columnsWithGaps.add(cell.ColumnIndex)
      destroyCubesPromises.push(cell.DestroyCube());
    }
    return Promise.all(destroyCubesPromises)
      .then(() => {
        this.MoveCubes(columnsWithGaps);

        GameManagerInstance.Turn(cellsCount);
      })
      .catch((error) => console.error(error));
  }

  ExplodeCircle(originCell) {
    const currentCellRowIndex = originCell.RowIndex;
    const currentCellColumnIndex = originCell.ColumnIndex;
    const cellsForExplode = new Stack();
    const radius = 2;

    for (let row = currentCellRowIndex - radius; row <= currentCellRowIndex + radius; row++) {
      for (let column = currentCellColumnIndex - radius; column <= currentCellColumnIndex + radius; column++) {
        const isPointInField =
          column < 0 ||
          row < 0 ||
          column > this.field.columns - 1 ||
          row > this.field.rows - 1;

        if (isPointInField) {
          continue;
        }
        const isPointInCircle =
          (row - currentCellRowIndex) * (row - currentCellRowIndex) +
          (column - currentCellColumnIndex) * (column - currentCellColumnIndex) <= radius * radius;

        if (isPointInCircle) {
          const pos = new CellPosition(column, row);
          const cell = this.grid.GetCell(pos);

          cellsForExplode.Push(cell);
        }
      }
    }
    this.DestroyCubes(cellsForExplode);
  }

  GetRowElements(originCell) {
    const currentRowIndex = originCell.RowIndex;
    const result = new Stack();

    for (let columnIndex = 0; columnIndex < this.field.columns; columnIndex++) {
      const cellPos = new CellPosition(columnIndex, currentRowIndex);
      const cell = this.grid.GetCell(cellPos);

      result.Push(cell);
    }
    return result;
  }

  GetColumnElements(originCell) {
    const currentColumnIndex = originCell.ColumnIndex;
    const result = new Stack();

    for (let rowIndex = 0; rowIndex < this.field.rows; rowIndex++) {
      const cellPos = new CellPosition(currentColumnIndex, rowIndex);
      const cell = this.grid.GetCell(cellPos);

      result.Push(cell);
    }
    return result;
  }

  // TODO: Not working, I don't know why
  ExplodeInChristStyle(originCell) {
    const rowElements = this.GetRowElements(originCell);
    const columnElements = this.GetColumnElements(originCell);
    const combinedStack = rowElements.Concat(columnElements);

    this.DestroyCubes(combinedStack);
  }

  SuperTileExplosion(originCell) {
    const cells = MathUtils.Random(0, 2) === 0 ?
      this.GetRowElements(originCell) :
      this.GetColumnElements(originCell);

    this.DestroyCubes(cells);
  }

  ApplyBooster(originCell) {
    switch (BoosterManagerInstance.CurrentBooster) {
      case BoosterType.Bomb: {
        this.ExplodeCircle(originCell);
        break;
      }
      case BoosterType.JesusBomb: {
        this.ExplodeInChristStyle(originCell);
        break;
      }
    }
    BoosterManagerInstance.ActivateBooster();
  }

  BurnCubes(originCell) {
    if (BoosterManagerInstance.CurrentBooster) {
      return this.ApplyBooster(originCell);
    }
    if (originCell.Cube instanceof SuperTile) {
      return this.SuperTileExplosion(originCell);
    }
    const cellsForBurning = this.GetNeighboursWithTheSameColor(originCell);

    if (cellsForBurning.Size() >= 2) {
      const cellsCount = cellsForBurning.Size();

      this.DestroyCubes(cellsForBurning).then(() => {
        if (cellsCount > 5) {
          originCell.CreateSuperTile();
        }
      });
    }
  }

}
