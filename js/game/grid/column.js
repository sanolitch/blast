import { Cell } from "./cell";
import { CellPosition } from "./cell-position";

export class Column {

  constructor(columnIndex, rowCount) {
    this.columnItems = [];
    this.columnIndex = columnIndex;

    this.InitColumn(rowCount);
  }

  get Items() {
    return this.columnItems;
  }

  get Count() {
    return this.columnItems.length;
  }

  InitColumn(rowCount) {
    for (let rowIndex = 0; rowIndex < rowCount; rowIndex++) {
      const position = new CellPosition(this.columnIndex, rowIndex);
      const newCell = new Cell(position);

      this.columnItems.push(newCell);
      newCell.CreateCube();
    }
  }

  GetItem(rowIndex) {
    return this.columnItems[rowIndex];
  }

  FillColumn() {
    for (const cell of this.Items) {
      if (cell.Cube) {
        continue;
      }
      cell.CreateCube();
    }
  }

  SwapItems(fromCell, toCell) {
    const cube = fromCell.Cube;

    toCell.Cube = cube;

    cube.UpdatePosition();
  }

  PushItems() {
    for (let i = 1; i < this.Count; i++) {
      for (let j = i; j > 0 && !this.Items[j].IsEmpty && this.Items[j - 1].IsEmpty; j--) {
        const fromCell = this.Items[j];
        const toCell = this.Items[j - 1];

        this.SwapItems(fromCell, toCell);
      }
    }
    this.FillColumn();
  }
}
