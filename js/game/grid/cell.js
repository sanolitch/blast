import { Cube } from "../game-objects/cube";
import { CellNeighbours } from "./cell-neighbours";
import { IsNull } from "../../engine/utils/object-utils";
import { SuperTile } from "../game-objects/super-tile";

export class Cell {

  constructor(position) {
    this.position = position;
    this.neighbours = new CellNeighbours(this.ColumnIndex, this.RowIndex);
    this.cube = null;
  }

  get Position() {
    return this.position;
  }

  get Neighbours() {
    return this.neighbours;
  }

  get ColumnIndex() {
    return this.Position.ColumnIndex;
  }

  get RowIndex() {
    return this.Position.RowIndex;
  }

  get Cube() {
    return this.cube;
  }

  set Cube(cube) {
    cube.cell.cube = null;
    this.cube = cube;
    this.cube.cell = this;
  }

  get IsEmpty() {
    return IsNull(this.cube);
  }

  CreateCube() {
    this.cube = new Cube(this);
  }

  CreateSuperTile() {
    this.cube = new SuperTile(this);
  }

  DestroyCube() {
    if (!this.cube) {
      return;
    }
    return this.cube.Destroy().then(() => this.cube = null);
  }

}
