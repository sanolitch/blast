import { BaseSceneObject } from "../../engine/base-objects/base-scene-object";
import { AnimatedSpriteSheet } from "../../engine/base-objects/animated-sprite-sheet";
import { AnimatorBuilder } from "../../engine/base-objects/animator-builder";
import { Position } from "../../engine/structures/position";
import { MathUtils } from "../../engine/utils/math-utils";
import { GridManagerInstance } from "../../main";

export const CubeColor = Object.freeze(
  {
    Blue: 1,
    Green: 2,
    Purple: 3,
    Red: 4,
    Yellow: 5
  }
)

export class Cube extends BaseSceneObject {

  constructor(cell) {
    super();

    this.field = GridManagerInstance.field;

    this.InitializeCube(cell);
  }

  GetImagePath() {
    switch (this.color) {
      case CubeColor.Blue: {
        return 'blue';
      }
      case CubeColor.Green: {
        return 'green';
      }
      case CubeColor.Purple: {
        return 'purple';
      }
      case CubeColor.Red: {
        return 'red';
      }
      case CubeColor.Yellow: {
        return 'yellow';
      }
    }
  }

  // TODO: This class should be independent
  UpdatePosition(velocityValue = 5) {
    const x = this.field.LeftBorderX + (this.cell.ColumnIndex * this.transform.Width);
    const y = this.field.LeftBorderY + (this.cell.RowIndex * this.transform.Height);
    const position = new Position(x, y);

    this.transform.Move(position, velocityValue);
  }

  GetAnimatorBuilder() {
    const animatorBuilder = new AnimatorBuilder();

    animatorBuilder.IsLoop = false;
    animatorBuilder.TotalFrames = 6;
    animatorBuilder.IsPlaying = false;
    animatorBuilder.AnimationSpeed = 0.2;

    return animatorBuilder;
  }

  InitializeCube(cell) {
    this.cell = cell;
    this.color = MathUtils.Random(1, 6);

    this.SetSize(this.field.cubeWidth, this.field.cubeHeight);

    this.sprite = new AnimatedSpriteSheet(this.transform, this.GetAnimatorBuilder());
    this.sprite.frameWidth = 171;
    this.sprite.frameHeight = 192;

    this.SetImagePath( `./img/game-objects/${this.GetImagePath()}_cube.png`);

    const x = this.field.LeftBorderX + (this.cell.ColumnIndex * this.transform.Width);
    const y = this.field.Height + this.field.LeftBorderY;
    const z = this.field.Height + this.field.fieldOffsetY;
    const position = new Position(x, y);

    this.SetPosition(position);
    this.UpdatePosition(15);
  }

  Destroy() {
    return new Promise((resolve, reject) => {
      this.sprite.animator.PlayOnce(() => {
        super.Destroy();

        resolve();
      });
    });
  }

}
