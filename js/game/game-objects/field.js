import { CubesGrid } from "../grid/cubes-grid";
import { Position } from "../../engine/structures/position";
import { UiCompositeObject } from "../../engine/ui/ui-composite-object";

export class Field extends UiCompositeObject {

  constructor() {
    super();

    this.columns = 7;
    this.rows = 8;
    this.cubeWidth = 85;
    this.cubeHeight = 95;
    this.fieldOffsetX = 20;
    this.fieldOffsetY = 20;
    this.fieldPositionX = 50;
    this.fieldPositionY = 150;

    const width = (this.columns * this.cubeWidth) + (this.fieldOffsetX * 2);
    const height = (this.rows * this.cubeHeight) + (this.fieldOffsetY * 2);
    const position = new Position(this.fieldPositionX, this.fieldPositionY, 0);

    this.SetPosition(position);
    this.SetSize(width, height);
    this.container.SetImagePath('img/game-objects/field.png');

    this.cubesGrid = new CubesGrid(this);

    this.container.AddObject(this.cubesGrid, new Position(0, 0, 1), 100, 100);
  }

  get Width() {
    return this.Transform.Width;
  }

  get Height() {
    return this.Transform.Height;
  }

  get LeftBorderX() {
    return this.fieldOffsetX + this.Transform.X;
  }

  get LeftBorderY() {
    return this.fieldOffsetY + this.Transform.Y;
  }

}
