import { Cube } from "./cube";

export class SuperTile extends Cube {

  constructor(cell) {
    super(cell);

    this.InitializeCube(cell);
  }

  InitializeCube(cell) {
    super.InitializeCube(cell);

    this.color = -1;
    this.sprite.animator.isPlaying = true;
    this.sprite.animator.isLoop = true;
    this.sprite.animator.totalFrames = 20;

    this.SetImagePath( `./img/game-objects/super_tile.png`);
  }

  Destroy() {
    return new Promise((resolve, reject) => {
      this.SetImagePath(`./img/game-objects/super_tile_explosion.png`);

      this.sprite.animator.frame = 0;
      this.sprite.animator.isLoop = false;
      this.sprite.animator.totalFrames = 6;

      this.sprite.animator.PlayOnce(() => {
        this.sprite.StopRender();

        resolve();
      });
    });
  }

}
