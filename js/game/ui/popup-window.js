import { UiCompositeObject } from "../../engine/ui/ui-composite-object";
import { UiButton } from "../../engine/ui/button";
import { UiManager } from "../popup-manager";
import { Position } from "../../engine/structures/position";

export class PopupWindowBuilder {

  constructor() {
    this.width = 670;
    this.height = 200;
    this.position = new Position( 35, 350);
  }

}

export class PopupWindow extends UiCompositeObject {

  constructor(popupBuilder) {
    super();

    this.SetSetting(popupBuilder);
    this.container.SetImagePath('/img/ui/score/inner_panel.png');

    this.button = new UiButton(true);
    this.button.container.SetImagePath('/img/ui/buttons/red_button.png');
    this.button.OnButtonClick = () => this.ClosePopup();

    this.text = this.CreateTextObj('', { x: 30, y: 70 });

    this.container.AddObject(this.button, { x: 270, y: 130 }, 18, 20);
    this.button.CreateTextObj('ok', { x: 45, y: 25, z: 99 }, '20px' );
    this.closeCallback = null;

    this.ClosePopup();
  }

  SetSetting(popupBuilder) {
    this.SetSize(popupBuilder.width, popupBuilder.height);
    this.SetPosition(popupBuilder.position);
  }

  ClosePopup() {
    this.container.Destroy();
    this.button.Destroy();
    this.text.Destroy();

    if (this.closeCallback) {
      this.closeCallback();

      this.closeCallback = null;
    }
  }

  Show(message) {
    this.container.sprite.StartRender();
    this.button.container.sprite.StartRender();
    this.text.StartRender();

    this.text.Text = message;
  }

  ShowModal(message, closeCallback) {
    this.container.sprite.StartRender();
    this.button.container.sprite.StartRender();
    this.text.StartRender();
    this.closeCallback = closeCallback;

    this.text.Text = message;
  }

}
