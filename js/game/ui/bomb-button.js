import { UiButton } from "../../engine/ui/button";
import { GameManager } from "../game-manager";
import { BoosterManagerInstance, GridManagerInstance } from "../../main";

export class BombButton extends UiButton {

  constructor() {
    super();

    this.SetSize(150, 150);
    this.InitButton();

    GameManager.Subscribe('win', this.UpdateBombsCount.bind(this));
    GameManager.Subscribe('loose', this.UpdateBombsCount.bind(this));
    GameManager.Subscribe('booster', this.UpdateBombsCount.bind(this));
  }

  InitButton() {
    this.container.SetImagePath('img/ui/buttons/bomb_button.png');

    this.SetPosition({ x: GridManagerInstance.field.Width + 100, y: GridManagerInstance.field.Height - 200 });
    this.CreateTextObj(BoosterManagerInstance.Bombs, { x: 70, y: 112 }, '20px');
  }

  UpdateBombsCount() {
    this.text.Text = BoosterManagerInstance.Bombs.toString();
  }

  OnButtonClick() {
    if (!super.OnButtonClick()) {
      return;
    }
    if (BoosterManagerInstance.CurrentBooster) {
      return BoosterManagerInstance.DefuseBomb();
    }
    BoosterManagerInstance.ChargeBomb();
  }

}
