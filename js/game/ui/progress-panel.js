import { Position } from "../../engine/structures/position";
import { ProgressBar } from "../../engine/ui/progress-bar/progress-bar";
import { UiCompositeObject } from "../../engine/ui/ui-composite-object";
import { GameManager } from "../game-manager";
import { GridManagerInstance } from "../../main";

export class ProgressPanel extends UiCompositeObject {

  constructor() {
    const field = GridManagerInstance.field;
    const containerPosition = new Position(field.fieldPositionX, 0);
    const containerWidth = field.fieldOffsetX * 2 + field.columns * field.cubeWidth;

    super(containerWidth, 120, containerPosition);

    this.CreateProgressBarPanel();
    this.SubscribeScoreChanging();
  }

  CalcShare(score, goal) {
    const percent = goal / 100;

    return score / percent;
  }

  UpdatePanel() {
    this.progressBar.Value = this.CalcShare(GameManager.Score, GameManager.Goal);
  }

  SubscribeScoreChanging() {
    GameManager.Subscribe('score', this.UpdatePanel.bind(this));
  }

  CreateProgressBarPanel() {
    this.progressBar = new ProgressBar(0, 0);

    this.container.SetImagePath('img/ui/progress-bar/progress_bar_panel.png');
    this.progressBar.SetImagePaths('img/ui/progress-bar/progress_bar_capacity.png',
      'img/ui/progress-bar/progress_bar_filling.png');
    const title = this.CreateTextObj('прогресс');

    this.container.AddObject(this.progressBar, new Position(40, 50), 85, 20);
    this.container.AddObject(title, new Position(240, 30));
  }

}
