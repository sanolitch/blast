import { BombButton } from "./bomb-button";
import { BoosterManagerInstance, GridManagerInstance } from "../../main";

export class JesusBombButton extends BombButton {

  constructor() {
    super();
  }

  InitButton() {
    this.container.SetImagePath('img/ui/buttons/jesus_bomb_button.png');

    this.SetPosition( { x: GridManagerInstance.field.Width + 400, y: GridManagerInstance.field.Height - 200 });
    this.CreateTextObj(BoosterManagerInstance.JesusBombs, { x: 70, y: 112 }, '20px');
  }

  UpdateBombsCount() {
    this.text.Text = BoosterManagerInstance.JesusBombs.toString();
  }

  OnButtonClick() {
    alert('Я не имею ни малейшего понятия почему, но после этой штуки не выполняется промис, ' +
      'и, как следтствие, не поднимаются кубики Т_Т, но выжигает как положено');

    BoosterManagerInstance.ChargeJesusBomb();
  }

}
