import { Position } from "../../engine/structures/position";
import { GameManager } from "../game-manager";
import { UiCompositeObject } from "../../engine/ui/ui-composite-object";
import { GameManagerInstance, GridManagerInstance } from "../../main";

export class ScorePanel extends UiCompositeObject {

  constructor() {
    super();

    this.CreateScorePanel();
    this.SubscribeScoreChanging();
  }

  UpdatePanel() {
    this.scoreText.Text = GameManagerInstance.score.toString();
    this.turnText.Text = GameManagerInstance.turnsLeft.toString();
    this.goalText.Text = GameManagerInstance.goal.toString();
    this.levelText.Text = GameManagerInstance.level.toString();
  }

  SubscribeScoreChanging() {
    GameManager.Subscribe('score', this.UpdatePanel.bind(this));
  }

  CreateCirclePanel() {
    const circlePanel = new UiCompositeObject();

    circlePanel.container.SetImagePath('/img/ui/score/circle_panel.png');
    this.container.AddObject(circlePanel, { x: 10, y: 10 }, 95, 60);

    this.turnText = this.CreateTextObj(GameManagerInstance.turnsLeft, { x: 123, y: 160 }, '60px');
  }

  CreateBigPanel() {
    const bigPanelPosition = new Position(GridManagerInstance.field.Width + 100, GridManagerInstance.field.fieldPositionY);

    this.container.SetPosition(bigPanelPosition)
    this.container.SetSize(300, 450);
    this.container.SetImagePath('/img/ui/score/rounded_panel_bright.png');
  }

  CreateInnerPanel() {
    const innerPanel = new UiCompositeObject();

    this.container.AddObject(innerPanel, { x: 30, y: 280 }, 80, 30);

    innerPanel.CreateTextObj('Очки:', { x: 15, y: 40 }, '22px');
    innerPanel.CreateTextObj('Нужно:', { x: 130, y: 40 }, '22px');
    innerPanel.CreateTextObj('Уровень:', { x: 15, y: 120 }, '22px');
    innerPanel.container.SetImagePath('/img/ui/score/inner_panel.png');

    this.scoreText = innerPanel.CreateTextObj(GameManagerInstance.score, { x: 15, y: 80 }, '25px');
    this.goalText = innerPanel.CreateTextObj(GameManagerInstance.goal, { x: 135, y: 80 }, '25px');
    this.levelText = innerPanel.CreateTextObj(GameManagerInstance.level, { x: 135, y: 120 }, '25px');
  }

  CreateScorePanel() {
    this.CreateBigPanel();
    this.CreateCirclePanel();
    this.CreateInnerPanel()
    this.UpdatePanel();
  }

}
