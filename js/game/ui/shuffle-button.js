import { UiButton } from "../../engine/ui/button";
import { GameManager } from "../game-manager";
import { BoosterManagerInstance, GridManagerInstance } from "../../main";

export class ShuffleButton extends UiButton {

  constructor() {
    super();

    this.container.SetImagePath('img/ui/buttons/shuffle_button.png');

    this.SetSize(150, 150);
    this.SetPosition({ x: GridManagerInstance.field.Width + 250, y: GridManagerInstance.field.Height - 200 });
    this.CreateTextObj(BoosterManagerInstance.Shuffles, { x: 70, y: 112 }, '20px');

    GameManager.Subscribe('score', this.UpdateShuffleCount.bind(this));
    GameManager.Subscribe('booster', this.UpdateShuffleCount.bind(this));
  }

  UpdateShuffleCount() {
    this.text.Text = BoosterManagerInstance.Shuffles.toString();
  }

  OnButtonClick() {
    if (!super.OnButtonClick()) {
      return;
    }
    BoosterManagerInstance.Shuffle();
  }

}
