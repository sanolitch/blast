const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  context: __dirname,
  devtool: "source-map",
  entry: './js/main.js',
  output: {
    path: __dirname + '/dist', // абсолютный путь к каталогу, в который мы хотим поместить вывод
    filename: 'game.bundle.js' // имя файла, который будет содержать наш вывод - мы могли бы назвать его как угодно, но типично bundle.js
  },
  devServer: {
    inline:true,
    port: 10000,
    watchContentBase: true,
    compress: true,
  },
  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader',
        ],
      },
      {
        test: /\.js$/,
        include: __dirname + '/js',
        use: {
          loader: 'babel-loader',
          options: { "presets": ["@babel/preset-env"] }
        }
      },
    ]
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        { from: 'img', to: 'img' }
      ]
    }),
    new HtmlWebpackPlugin({template: "index.html"})
  ]
};

